﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;

[Serializable]
public class LoggingManager : MonoBehaviour
{
    #region Singleton
    private static LoggingManager _instance;

    public static LoggingManager Instance
    {
        get
        {
            // If there is no instance of this GameObject, make one
            if (_instance == null)
            {
                GameObject obj = new GameObject("Logging");
                Instantiate(obj);
            }

            return _instance;
        }
    }
    #endregion

    Dictionary<string, int> property = new Dictionary<string, int>();
    [SerializeField]
    string path;
    // Use this for initialization
    void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        path = Application.persistentDataPath + "/properties_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + "_" + Guid.NewGuid().ToString() + ".txt";
        if (File.Exists(path))
        {
            Debug.LogWarning("Duplicate file encoutered, deleting");
            File.Delete(path);
        }
        StreamWriter file = new StreamWriter(path);
        file.Close();
    }

    /// <summary>
    /// Adds a property to the dictionary (all properties start with a capital letter).
    /// </summary>
    /// <param name="key">The name for the property</param>
    /// <param name="value">Amount to increase the property with. Can be negative, defaults to 1</param>
    public void AddToDictionary(string key, int value = 1)
    {
        if(property.ContainsKey(key))
        {
            property[key] += value;
        }
        else
        {
            try
            {
                property.Add(key, value);
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
        WriteToFile();
    }

    public int GetDictionaryValue(string key)
    {
        return property[key];
    }

    private void WriteToFile()
    {
        //if we don't have a path we can't write to a file
        if (path == null || path.Length == 0)
            return;

        StringBuilder builder = new StringBuilder();
        foreach (KeyValuePair<string, int> K in property)
        {
            builder.Append("Key: " + K.Key + "  Value: " + K.Value);
            builder.AppendLine();
            //Debug.Log(K.Key + " times: " + K.Value);

        }
        StreamWriter file = new StreamWriter(path);
        file.WriteLine(builder);
        file.Close();
    }

    private void OnApplicationQuit()
    {
        //if we don't have a path we can't write to a file
        if (path == null || path.Length == 0)
            return;
        StreamWriter file = new StreamWriter(path, append: true);
        file.WriteLine("Game closed after: " + Time.time + " Playtime, end date-time is: " + DateTime.Now.ToString());
        file.Close();
        Debug.Log("Game closed after: " + Time.time + " Playtime, end date-time is: " + DateTime.Now.ToString());
    }

}

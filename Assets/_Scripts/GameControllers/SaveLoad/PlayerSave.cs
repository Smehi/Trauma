﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSave : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (SaveLoadManager.saveLoadManager.CheckSaveExists())
            {
                SaveLoadManager.saveLoadManager.Delete();
                Debug.Log("Deleting current save");
            }
            else if (!SaveLoadManager.saveLoadManager.CheckSaveExists())
            {
                Debug.LogError("Cannot delete save file because it does not exist.");
            }
        }

        if (Input.GetButtonDown("Save"))
        {
            SavePosition();

            // Very important to have this on the last line because it needs to be called after all values have been adjusted
            SaveLoadManager.saveLoadManager.Save();
        }

        if (Input.GetButtonDown("Load"))
        {
            if (SaveLoadManager.saveLoadManager.CheckSaveExists())
            {
                // Very important t have this on the first line because it need to be called before any values are going to be adjusted
                SaveLoadManager.saveLoadManager.Load();

                LoadPosition();
            }
            else
            {
                Debug.LogError("Cannot load because save file does not exist");
            }
        }
	}

    private void SavePosition()
    {
        SaveLoadManager.saveLoadManager.playerPositionX = transform.position.x;
        SaveLoadManager.saveLoadManager.playerPositionY = transform.position.y;
        SaveLoadManager.saveLoadManager.playerPositionZ = transform.position.z;

        Debug.Log("Saving position: " + transform.position);
    }

    private void LoadPosition()
    {
        transform.position = new Vector3(SaveLoadManager.saveLoadManager.playerPositionX,
                                         SaveLoadManager.saveLoadManager.playerPositionY,
                                         SaveLoadManager.saveLoadManager.playerPositionZ);

        Debug.Log("Loading last save!");
        Debug.Log("Loaded position: " + transform.position);
    }
}

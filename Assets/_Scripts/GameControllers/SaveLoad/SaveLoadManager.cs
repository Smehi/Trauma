﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoadManager : MonoBehaviour {

    public static SaveLoadManager saveLoadManager;

    public float playerPositionX, playerPositionY, playerPositionZ;

    private void Awake()
    {
        if (saveLoadManager == null)
        {
            DontDestroyOnLoad(gameObject);
            saveLoadManager = this;
        }
        else if (saveLoadManager != this)
        {
            Destroy(gameObject);
        }
    }

    public void Save()
    {
        // Create BinaryFormatter and a file to save to
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/player.save");

        // Save the data
        PlayerData data = new PlayerData();
        data.SavePosition();

        // Write data to file and close file
        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if (CheckSaveExists())
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/player.save", FileMode.Open);

            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            data.LoadPosition();
        }
    }

    public void Delete()
    {
        if (File.Exists(Application.persistentDataPath + "/player.save"))
        {
            File.Delete(Application.persistentDataPath + "/player.save");
        }
    }

    public bool CheckSaveExists()
    {
        if (File.Exists(Application.persistentDataPath + "/player.save"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

[Serializable]
class PlayerData
{
    public float playerPosX, playerPosY, playerPosZ;

    public void SavePosition()
    {
        playerPosX = SaveLoadManager.saveLoadManager.playerPositionX;
        playerPosY = SaveLoadManager.saveLoadManager.playerPositionY;
        playerPosZ = SaveLoadManager.saveLoadManager.playerPositionZ;
    }

    public void LoadPosition()
    {
        SaveLoadManager.saveLoadManager.playerPositionX = playerPosX;
        SaveLoadManager.saveLoadManager.playerPositionY = playerPosY;
        SaveLoadManager.saveLoadManager.playerPositionZ = playerPosZ;
    }
}

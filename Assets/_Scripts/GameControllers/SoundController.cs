﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    #region Singleton

    public static SoundController instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of SoundController found!");
            return;
        }

        instance = this;
    }

    #endregion

    public AudioClip ambientSound;
    public List<AudioClip> ambientSoundFX = new List<AudioClip>();
    [Range(0,1)]
    public float ambientSoundfxVolume = 1f;

    [Range(0, 600)]
    public float ambientSoundfxTimerMax = 10f;
    [Range(0, 600)]
    public float ambientSoundfxTimerMin = 5f;
    public float timeTillNextAmbientSound;

    public GameObject soundEmmitterPrefab;

    private AudioSource myAudioSource;

    public Vector3 minDistance = Vector3.zero;
    public Vector3 maxDistance = Vector3.zero;

	// Use this for initialization
	void Start () {
        myAudioSource = GetComponent<AudioSource>();
        myAudioSource.volume = ambientSoundfxVolume;
        timeTillNextAmbientSound = GetRandomTime();
        myAudioSource.loop = true;
        myAudioSource.clip = ambientSound;
        myAudioSource.Play();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        timeTillNextAmbientSound -= Time.fixedDeltaTime;
        if(timeTillNextAmbientSound <= 0)
        {
            timeTillNextAmbientSound = GetRandomTime();
            PlayNextAmbientSound();
        }
    }

    [ContextMenu("Play next ambient sound")]
    private void PlayNextAmbientSound()
    {
        this.PlayAmbientSound(GetRandomSoundFile(ambientSoundFX));
    }

    /// <summary>
    /// Gets a random audio clip from a list
    /// </summary>
    /// <param name="list">The list to pick from</param>
    /// <returns>A random clip in said list</returns>
    public AudioClip GetRandomSoundFile(List<AudioClip> list)
    {
        int index = Random.Range(0, list.Count);
        return list[index];
    }


    private float GetRandomTime()
    {
        return timeTillNextAmbientSound = ambientSoundfxTimerMin + Random.Range(0, ambientSoundfxTimerMax - ambientSoundfxTimerMin);
    }

    /// <summary>
    /// Gets a random possition that belongs to the ambient sound range
    /// </summary>
    /// <returns>AmbientSound range possition</returns>
    private Vector3 GetRandomAmbientPosition()
    {
        return new Vector3(
            Random.Range(minDistance.x, maxDistance.x - minDistance.x),
            Random.Range(minDistance.y, maxDistance.y - minDistance.y),
            Random.Range(minDistance.z, maxDistance.z - minDistance.z));
    }


    /// <summary>
    /// Plays a sound as if it's an ambient sound(following the random distance params of this object)
    /// </summary>
    /// <param name="audio">The audio file to play</param>
    public void PlayAmbientSound(AudioClip audio)
    {
        var SE = Instantiate(soundEmmitterPrefab, GetRandomAmbientPosition(), Quaternion.identity);
        SE.GetComponent<SoundEmmitter>().SetupEmitter(audio);
        SE.GetComponent<AudioSource>().volume = ambientSoundfxVolume;
    }


    /// <summary>
    /// Plays a sound at a specified location
    /// </summary>
    /// <param name="audio">Audio file to play</param>
    /// <param name="location">Location from which the file will be played from</param>
    public void PlaySoundAtLocation(AudioClip audio, Vector3 location)
    {
        var SE = Instantiate(soundEmmitterPrefab, location, Quaternion.identity);
        SE.GetComponent<SoundEmmitter>().SetupEmitter(audio);
    }

}

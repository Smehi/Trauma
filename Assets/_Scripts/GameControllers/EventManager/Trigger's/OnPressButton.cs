﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OnPressButton : EventManager.TriggerEvent
{
    public void Start()
    {
        EventManager.triggerEvents[myId] = this;
    }
    public override void getTriggered()
    {
        triggered = true;
        EventManager.makeAct(myId);
    }
    void OnTriggerStay() {
        if (Input.GetKeyDown(KeyCode.E))
        {
            getTriggered();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoAudio : EventManager.DoEvent
{
    public bool Do;
    public void Start()
    {
        EventManager.doEvents[myId] = this;
    }
    public GameObject AudioObject;
    public override void act()
    {
        AudioObject.SetActive(true);
        Do = true;
        EventManager.doIDs[myId] = true;
        EventManager.makeAct();
    }
}

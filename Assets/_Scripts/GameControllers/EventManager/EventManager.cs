﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;
    public static class EventManager
    {
        public static bool[] doIDs = new bool[20];
        public static TriggerEvent[] triggerEvents = new TriggerEvent[20];
        public static DoEvent[] doEvents = new DoEvent[20];
        public abstract class TriggerEvent : MonoBehaviour
        {

            public int myId = 0;
            public bool triggered = false;
            public abstract void getTriggered();

    }
        public abstract class DoEvent : MonoBehaviour
        {

            public int myId = 0;
            public abstract void act();

    }
        public static void makeAct(int triggerID = 0 ) {

            int currentState = StateMachine<StateUser>.currentStateCount;
        Debug.Log("MakeAct  " + triggerID + "  " + currentState);
        int currentRoomID = StateMachine<StateUser>.currentRoomID;
        switch (currentState) {
            case 0:
                if (!doIDs[1])
                    doEvents[1].act();
                break;
            case 1:
                if (currentRoomID == 5 && !doIDs[2])
                    doEvents[2].act();
                if (triggerID == 3 && !doIDs[3])
                    doEvents[3].act();
                break;
            case 2:
                if (!doIDs[4])
                    doEvents[4].act();
                if (triggerID == 4 && !doIDs[5])
                {
                    doEvents[5].act();
                    doEvents[6].act();
                }
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                default:
                    break;
            }
        }
    }

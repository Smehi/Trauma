﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MoveBackManager : MonoBehaviour {

    #region Singleton Part
    private static MoveBackManager _instance;
    public static MoveBackManager Instance // This is what is being called to access everything in this GameObject
    {
        get
        {
            // If there is no instance of this GameObject, make one
            if (_instance == null)
            {
                GameObject obj = new GameObject("MoveBackCollider");
                Instantiate(obj);
            }

            return _instance;
        }
    }
    #endregion

    public delegate void ChangePositionAndRotation();
    public static event ChangePositionAndRotation onCollideBox;

    public bool Collided { get; set; }

    void Awake()
    {
        _instance = this;

        // Initializing of the box
        _instance.gameObject.transform.position = new Vector3(0.35f, -3, -4.2f);
        _instance.gameObject.AddComponent<BoxCollider>();
        _instance.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        _instance.gameObject.GetComponent<BoxCollider>().size = new Vector3(5, 4, 1);

        Collided = false; // Set to false to collision is possible
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !Collided)
        {
            Collided = true; // Set to true so collision is not constant but rather a trigger
            LoggingManager.Instance.AddToDictionary("TimesLoopedHouse");

            if (onCollideBox != null)
            {
                onCollideBox();
            } 
        }
    }
}

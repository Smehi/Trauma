﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillBox : MonoBehaviour
{
    public List<string> whiteListTags = new List<string>();

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("KILL " + other.name);

        if (whiteListTags.Contains(other.tag))
            return;
        Destroy(other.gameObject);
    }
}

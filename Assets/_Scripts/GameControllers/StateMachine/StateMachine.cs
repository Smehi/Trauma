﻿using StateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace StateMachine
{
    public class StateMachine<T>
    {
        public static int currentStateCount = -1;
        int maxrooms;
        public State<T>[] states = new State<T>[40];
        public State<T> currentState { get; private set; }
        public static Room<T> currentRoom { get; private set; }
        public static int currentRoomID;

        public T Owner;

        public StateMachine(T _owner)
        {
            Owner = _owner;
            currentState = null;
            currentRoom = null;
        }

        public void ChangeState(State<T> _newstate)
        {
            if (currentState != null)
                currentState.ExitState(Owner);
            currentState = _newstate;
            currentStateCount++;
            Debug.Log(currentStateCount);
            states[currentStateCount] = _newstate;
            currentState.EnterState(Owner);
            EventManager.makeAct();
        }

        public void ChangeRoom(Room<T> _newRoom, int roomID)
        {
            if (currentRoom != null)
                currentRoom.ExitRoom(Owner);
            if (currentState.CheckIfExists(roomID))
            {
                Debug.Log("Existing Room room ID:" + roomID);
                currentRoomID = roomID;
                currentRoom = currentState.GetRoom(roomID);
                currentRoom.EnterRoom(Owner);
            }
            else {
                Debug.Log("New Room room ID:"+ roomID);
                currentRoom = _newRoom;
                currentRoomID = roomID;
                currentState.AddRoom(currentRoom, roomID);
                currentRoom.EnterRoom(Owner);
            }
            EventManager.makeAct();
        }

        public void Update()
        {
            if (currentState != null)
                currentState.UpdateState(Owner);
            if (currentRoom != null)
                currentRoom.UpdateRoom(Owner);
        }
    }

    public abstract class Room<T>
    {
        public abstract int GetID();
        public abstract void SetID(int roomID);
        public abstract void EnterRoom(T _owner);
        public abstract void ExitRoom(T _owner);
        public abstract void UpdateRoom(T _owner);
    }

    public abstract class State<T>
    {
        public abstract int GetID();
        public abstract void EnterState(T _owner);
        public abstract void ExitState(T _owner);
        public abstract void UpdateState(T _owner);
        public abstract void AddRoom(Room<T> room, int roomID);
        public abstract Room<T> GetRoom(int roomID);
        public abstract bool CheckIfExists(int roomID);
    }
}
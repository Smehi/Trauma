﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;

public class StateUser : MonoBehaviour
{
    public static StateMachine<StateUser> stateMachine { get; set; }
    public static bool switchState = false;
    public static int stateID = 0;

    void Start()
    {
        stateMachine = new StateMachine<StateUser>(this);
        stateMachine.ChangeState(new AState(stateID++));
    }
    public static void NextState() {
        stateMachine.ChangeState(new AState(stateID++));
    }
    public static void ChangeRoom(int iD) {
        stateMachine.ChangeRoom(Room.Instance, iD);
    }
}

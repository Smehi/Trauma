﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;
using System;

public class Room : Room<StateUser>
{
    private static Room _instance;
    private int myID;
    float timeInRoom;
    int TimesEntered;
    bool inRoom;
    public Room()
    {

        if (_instance != null)
        {
            return;
        }

        _instance = this;
    }

    public static Room Instance
    {
        get
        {
            if (_instance == null)
            {
                new Room();
            }

            return _instance;
        }
    }
    public override void EnterRoom(StateUser _owner)
    {
        Debug.Log("Enter Room");
        TimesEntered += 1;
        inRoom = true;
    }

    public override void ExitRoom(StateUser _owner)
    {
        Debug.Log("Exit Room");
        inRoom = false;
    }

    public override void UpdateRoom(StateUser _owner)
    {
        timeInRoom += 1 / 60;
    }

    public override int GetID()
    {
        return myID;
    }

    public override void SetID(int roomID)
    {
        myID = roomID;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;
using System;

public class AState : State<StateUser>
{
    float timeInState;
    public Room<StateUser>[] rooms = new Room<StateUser>[5];
    public int[] roomIDs = new int[5];
    int maxRooms = -1;
    private int myID;
     public AState(int iD)
    {
        myID = iD;
        timeInState = 0;
    }
    public override void AddRoom(Room<StateUser> room, int roomID) {
        maxRooms++;
        rooms[maxRooms] = room;
        roomIDs[maxRooms] = roomID;
    }
    public override Room<StateUser> GetRoom(int roomID)
    {
        for (int i = 0; i < 5; i++) {
            if (roomIDs[i] == roomID) {
                return rooms[i];
            }
        }
        return rooms[0];
    }
    public override bool CheckIfExists(int roomID)
    {
        for (int i = 0; i < 5; i++)
        {
            if (roomIDs[i] == roomID)
            {
                return true;
            }
            
        }
        return false;
    }
    public override void EnterState(StateUser _owner)
    {
        Debug.Log("Enter State");
    }

    public override void ExitState(StateUser _owner)
    {
        Debug.Log("Exit State");
    }

    public override void UpdateState(StateUser _owner)
    {
        timeInState += 1 / 60;
    }

    public override int GetID()
    {
        return myID;
    }


}

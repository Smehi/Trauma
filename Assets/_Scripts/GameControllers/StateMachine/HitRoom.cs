﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitRoom : MonoBehaviour {
    public int roomID = 0;
    void OnTriggerEnter() {
        StateUser.ChangeRoom(roomID);
    }
}

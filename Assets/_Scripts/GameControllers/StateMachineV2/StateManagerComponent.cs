﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMananger;

public class StateManagerComponent : MonoBehaviour {

    #region Singleton

    public static StateManagerComponent instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }

        instance = this;
    }

    #endregion

    public SpawnNewHouse houseSpawnScript;
    public string currentStateName = "none";
    public GameObject agent;
    public StateManager<GameObject> stateMachine;
    public GameObject stateManangerObject = null;
    public DateTime currentStateEnteredDateTime;
	// Use this for initialization
	void Start () {
        currentStateEnteredDateTime = DateTime.Now;
        houseSpawnScript = this.GetComponent<SpawnNewHouse>();
        if (stateManangerObject == null)
        {
            stateMachine = new StateManager<GameObject>(this.transform.gameObject);
        }
        else
        {
            stateMachine = new StateManager<GameObject>(stateManangerObject);
        }
        //create all states
        var startState = new StartState("Start", "House prefabs/StartingHouse");
        var loopIntroductionState = new LoopIntroductionState("LoopIntroduction", "House prefabs/LoopIntroductionHouse");
        var teddyOutsideState = new TeddyOutsideState("TeddyOutside", "House prefabs/TeddyOutsideHouse");
        var teddyInsideState = new TeddyInsideState("TeddyInside", "House prefabs/TeddyInsideHouse");
        var clockState = new ClockState("Clock", "House prefabs/ClockHouse");
        var enterRoomsState = new EnterRoomsState("EnterRooms", "House prefabs/EnterRoomsHouse");
        var chessPuzzleBishopState = new ChessPuzzleBishopState("BishopPuzzle", "House prefabs/ChessPuzzleBishop");
        var chessPuzzleQueenStateHard = new ChessPuzzleQueenStateHard("QueenPuzzleHard", "House prefabs/ChessPuzzleQueenHard");
        var chessPuzzleQueenStateHardMoreTips = new ChessPuzzleQueenStateHardMoreTips("QueenPuzzleHardMoreTips", "House prefabs/ChessPuzzleQueenHardMoreTips");
        var chessPuzzleQueenEasy = new ChessPuzzleQueenStateEasy("QueenPuzzleEasy", "House prefabs/ChessPuzzleQueenEasy");
        

        //add exit states to states that need exit states
        startState.AddExitState(loopIntroductionState);
        loopIntroductionState.AddExitState(clockState);
        clockState.AddExitState(chessPuzzleBishopState);
        chessPuzzleBishopState.AddExitState(teddyOutsideState);
        teddyOutsideState.AddExitState(enterRoomsState);
        enterRoomsState.AddExitState(chessPuzzleQueenStateHard);

        chessPuzzleQueenStateHard.AddExitState(teddyInsideState);
        chessPuzzleQueenStateHard.AddExitState(chessPuzzleQueenStateHardMoreTips);

        chessPuzzleQueenStateHardMoreTips.AddExitState(teddyInsideState);
        chessPuzzleQueenStateHardMoreTips.AddExitState(chessPuzzleQueenEasy);
        
        chessPuzzleQueenEasy.AddExitState(teddyInsideState);

        //add all states too the state machine
        stateMachine.AddState(startState, true);
        stateMachine.AddState(loopIntroductionState);
        stateMachine.AddState(enterRoomsState);
        stateMachine.AddState(teddyOutsideState);
        stateMachine.AddState(teddyInsideState);
        stateMachine.AddState(clockState);
        stateMachine.AddState(chessPuzzleBishopState);
        stateMachine.AddState(chessPuzzleQueenEasy);
        stateMachine.AddState(chessPuzzleQueenStateHard);
        stateMachine.AddState(chessPuzzleQueenStateHardMoreTips);

        //enable the state machine
        stateMachine.EnableStateMachine();

        //Force the load of the first state
        startState.StartAsyncLoad();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if(stateMachine != null && stateMachine.isValidStateMachine)
            currentStateName = stateMachine._currentState._stateName;
	}

    /// <summary>
    /// Returns the current state string in a specific format used for logging stuff
    /// </summary>
    /// <returns>The string of the current state along with the time worked into it</returns>
    public string CurrentStateString()
    {
        return this.currentStateName + "_EnterTime:" + this.currentStateEnteredDateTime.ToString();
    }
}

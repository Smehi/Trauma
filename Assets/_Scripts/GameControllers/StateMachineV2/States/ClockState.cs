﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMananger;
using System;

public class ClockState : UnityState<GameObject>
{
    public ClockState(string name, string PrefabLocationString) : base(name, PrefabLocationString)
    {
    }

    public override bool EvaluateAgent(GameObject agent, out State<GameObject> changeStateToo)
    {
        SetClock clock = GameObject.FindGameObjectWithTag("Clock").GetComponent<SetClock>();
        changeStateToo = null;
        if (clock.TimeCorrect)
        {
            changeStateToo = exitStates["BishopPuzzle"];
            return true;
        }
        return false;
    }

    public override void OnEnterState(State<GameObject> prevState)
    {
        base.OnEnterState(prevState);
    }

    public override void OnStayInState(GameObject agent)
    {
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMananger;
using System;

public class StartState : UnityState<GameObject>
{
    public StartState(string name, string PrefabLocationString) : base(name, PrefabLocationString)
    {
    }

    public override bool EvaluateAgent(GameObject agent, out State<GameObject> changeStateToo)
    {
        changeStateToo = null;
        if (LoggingManager.Instance.GetDictionaryValue(StateManagerComponent.instance.CurrentStateString()) == 1)
        {
            changeStateToo = exitStates["LoopIntroduction"];
            return true;
        }
        return false;
    }

    public override void OnEnterState(State<GameObject> prevState)
    {
        base.OnEnterState(prevState);
    }


    public override void OnStayInState(GameObject agent)
    {
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMananger;
using System;

public class ChessPuzzleQueenStateHard : UnityState<GameObject>
{
    public ChessPuzzleQueenStateHard(string name, string PrefabLocationString) : base(name, PrefabLocationString)
    {
    }

    public override bool EvaluateAgent(GameObject agent, out State<GameObject> changeStateToo)
    {
        var chessboards = GameObject.FindGameObjectsWithTag("ChessBoard");
        foreach (GameObject chessboard in chessboards)
        {
            var chessboardPuzzleComponent = chessboard.GetComponent<SjorsGielen.Chessboard.ChessboardPuzzle>();
            if (chessboardPuzzleComponent.completed)
            {
                changeStateToo = exitStates["TeddyInside"];//replace with state appropriate
                return true;
            }
        }
        if (LoggingManager.Instance.GetDictionaryValue(StateManagerComponent.instance.CurrentStateString()) == 3)
        {
            changeStateToo = exitStates["QueenPuzzleHardMoreTips"];//change state too the easier state
            return true;
        }
        changeStateToo = null;
        return false;
    }

    public override void OnEnterState(State<GameObject> prevState)
    {
        base.OnEnterState(prevState);
    }

    public override void OnStayInState(GameObject agent)
    {
    }

}

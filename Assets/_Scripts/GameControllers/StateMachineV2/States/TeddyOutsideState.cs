﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMananger;
using System;

public class TeddyOutsideState : UnityState<GameObject>
{
    public TeddyOutsideState(string name, string PrefabLocationString) : base(name, PrefabLocationString)
    {
    }

    public override bool EvaluateAgent(GameObject agent, out State<GameObject> changeStateToo)
    {
        changeStateToo = exitStates["EnterRooms"];
        return true;
    }

    public override void OnEnterState(State<GameObject> prevState)
    {
        base.OnEnterState(prevState);
    }


    public override void OnStayInState(GameObject agent)
    {
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMananger;
using System;

public class TeddyInsideState : UnityState<GameObject>
{
    public TeddyInsideState(string name, string PrefabLocationString) : base(name, PrefabLocationString)
    {
    }

    public override bool EvaluateAgent(GameObject agent, out State<GameObject> changeStateToo)
    {
        changeStateToo = null;
        return false;
    }

    public override void OnEnterState(State<GameObject> prevState)
    {
        base.OnEnterState(prevState);
    }

    public override void OnStayInState(GameObject agent)
    {
    }

}

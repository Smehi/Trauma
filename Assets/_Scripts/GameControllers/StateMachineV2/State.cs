﻿using System.Collections.Generic;

namespace StateMananger
{
    /* Improvements:
     * Alter the state class to no longer require instance of T
     * and exitStates
     */
    public abstract class State<T> : StateInterface<T>
    {
        public Dictionary<string, State<T>> exitStates;
        private string stateName;

        public string _stateName
        {
            get { return this.stateName; }
        }

        public State(string name)
        {
            this.exitStates = new Dictionary<string, State<T>>();
            this.stateName = name;
        }

        public virtual bool EvaluateAgent(T agent, out State<T> changeStateToo)
        {
            changeStateToo = null;
            return false;
        }

        public abstract void OnExitState(State<T> nextState);

        public abstract void OnEnterState(State<T> prevState);

        public abstract void OnStayInState(T agent);

        public void AddExitState(State<T> stateToAdd)
        {
            this.exitStates.Add(stateToAdd._stateName, stateToAdd);
        }

    }
}
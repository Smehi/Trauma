﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;


namespace StateMananger
{
    public abstract class UnityState<T> : State<T>
    {

        public string _prefabLocationString { get { return prefabLocationString; } }
        private Dictionary<string, GameObject> exitPrefabs;
        private string prefabLocationString;

        public UnityState(string name, string PrefabLocationString) : base(name)
        {
            prefabLocationString = PrefabLocationString;
        }

        public override void OnEnterState(State<T> prevState)
        {
            StartAsyncLoad();
        }

        public override void OnExitState(State<T> nextState)
        {
            StateManagerComponent.instance.houseSpawnScript.newHouse = exitPrefabs[nextState._stateName];
        }

        public void StartAsyncLoad()
        {
            StateManagerComponent.instance.StartCoroutine(AsyncLoadExitPrefabs());
        }

        IEnumerator AsyncLoadExitPrefabs()
        {
            var startTime = DateTime.Now;
            StringBuilder loadDebugOutput = new StringBuilder();
            exitPrefabs = new Dictionary<string, GameObject>();
            List<string> output = new List<string>();
            foreach (KeyValuePair<string, State<T>> keyValuePair in exitStates)
            {
                var timeStartLoad = DateTime.Now;
                UnityState<T> state = (UnityState<T>)keyValuePair.Value;
                if (state == null)
                    continue;
                var currentLoadProcess = Resources.LoadAsync(state.prefabLocationString);
                yield return currentLoadProcess;
                exitPrefabs.Add(state._stateName, currentLoadProcess.asset as GameObject);
                output.Add(String.Format("\nLoaded {0,30} succesfully. Time taken: {1,10} milliseconds", exitPrefabs[state._stateName].name, (DateTime.Now - timeStartLoad).Milliseconds));
            }
            var timeTaken = DateTime.Now - startTime;
            loadDebugOutput.AppendFormat("Finished loading in {0,5} milliseconds, Loaded:", timeTaken.TotalMilliseconds);
            foreach(string outputString in output)
            {
                loadDebugOutput.Append(outputString);
            }
            Debug.Log(loadDebugOutput.ToString());
        }
    }
}

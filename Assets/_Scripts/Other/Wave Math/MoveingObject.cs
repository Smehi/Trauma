﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts;

public class MoveingObject : MonoBehaviour {

    public WaveForm waveForm = WaveForm.sin;

    public float baseStart = 0.0f;
    public float amplitude = 1.0f;
    public float phase = 0.0f;
    public float frequency = 0.05f;
    [Range(0, 5)] public float polynomial = 2f;

    public Vector3 moveToPossition;
    public Vector3 curPossition;
    public Vector3 startPossition;
    public Vector3 possitiveEndPossition;
    public Vector3 negativeEndPossition;
    // Use this for initialization
    void Start ()
    { 
        startPossition = this.transform.localPosition;
        possitiveEndPossition = this.transform.localPosition + moveToPossition;
        negativeEndPossition = this.transform.localPosition - moveToPossition;
    }
	
	// Update is called once per frame
	void Update () {
        var distance = WaveMathHelper.EvalWave(phase, frequency, amplitude, baseStart, waveForm, polynomial);
        curPossition = startPossition + moveToPossition * distance;
        this.transform.localPosition = curPossition;
    }
}

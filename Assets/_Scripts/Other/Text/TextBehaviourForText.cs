﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextBehaviourForText : MonoBehaviour {

    Text i;

	// Use this for initialization
	void Start () {
        i = GetComponent<Text>();
        i.color = new Color(0, 0, 0, i.color.a);
    }
	
	// Update is called once per frame
	void Update () {
        if (i.color.a > 0)
        {
            i.color -= new Color(0, 0, 0, 0.02f);
        }
        else
        {
            i.color = new Color(0, 0, 0, 0);
        }
    }

    
}

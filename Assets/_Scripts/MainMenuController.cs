﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    public Button startButton;
    public Button closeButton;

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartButtonPress()
    {
        PersisntSceneManagementComponent.instance.LoadSceneWithLoadScreen("PlayingScene");
    }

    public void CloseButtonPress()
    {
        Application.Quit();
    }

}

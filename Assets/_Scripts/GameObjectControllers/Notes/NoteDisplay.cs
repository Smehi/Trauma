﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteDisplay : MonoBehaviour
{
    public Note note;

    public Text hint;

    // Use this for initialization
    void Start()
    {
        if(note != null)
            hint.text = note.hint;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnNewHouse : MonoBehaviour
{
    public GameObject newHouse;
    public Vector3 instantiatePosition;
    public Vector3 instantiateRotation;
    public GameObject currentHouse;
    public GameObject prevHouse;

    private bool cloned = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !cloned)
        {
            cloned = !cloned;
            MoveBackManager.onCollideBox += DeleteHouse;
            StateManagerComponent.instance.stateMachine.ExecuteCurrentState();
            prevHouse = currentHouse;
            currentHouse = Instantiate(newHouse, instantiatePosition, Quaternion.Euler(instantiateRotation));
        }
    }

    void DeleteHouse()
    {
        MoveBackManager.onCollideBox -= DeleteHouse;
        Destroy(prevHouse);
        cloned = !cloned;
    }
}

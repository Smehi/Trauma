﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetClock : MonoBehaviour
{
    #region Public Variables
    // The parent object for both handles (in this case the empty which the actual handle is in)
    public GameObject hoursObj;
    public GameObject minutesObj;

    // The object from the canvas that will be used to display the values
    public GameObject hoursValueTextObj;
    public GameObject minutesValueTextObj;

    // The object from the canvas that will be used to display the instructions
    public GameObject InstructionsTextObj;

    // The object for the camera
    public GameObject clockCamObj;

    // [0] = Inactive; [1] = Active, used for highlighting the handle that is currently active
    public Material[] materials = new Material[(int)HandleRenderer.max];

    public enum CurrentHandle { hours = 1, minutes = 2 }
    public CurrentHandle currentHandle = CurrentHandle.hours;

    public bool interactable = true;

    [Tooltip("How many times the value should change per second.")]
    public float valueChangeRate;
    [Tooltip("The amount that gets added to the \"valueChangeRate\" every time the value gets changed.")]
    public float addToValueChangeRate;
    [Tooltip("Maximum amount of time the of a handle can be changed per second. If \"valueChangeRate\" is higher than the max it will be set to max.")]
    public float maxValueChangeRate;
    #endregion

    #region Private Variables
    private GameObject player;

    // Used to determine the "correct" time state of the clock
    [SerializeField] [Range(1, 12)] private int hoursDesiredValue;
    [SerializeField] [Range(0, 59)] private int minutesDesiredValue;
    private bool hoursCorrect = false, minutesCorrect = false;
    private bool timeCorrect = false;

    // This is used to actually access the Text component
    // This won't be needed anymore when we have an actual clock with numbers
    private Text hoursValueText;
    private Text minutesValueText;

    // The renderer for both handles
    private Renderer hoursRenderer;
    private Renderer minutesRenderer;
    private enum HandleRenderer { inactive, active, max }
    private int inactive, active, correct;

    private int hoursCurrentValue, hoursMinValue, hoursMaxValue;
    private int minutesCurrentValue, minutesMinValue, minutesMaxValue;
    private bool toggled = false;

    private float originalValueChangeRate;
    private float nextTimeToChange = 0;
    #endregion

    // Use this for initialization
    void Start()
    {
        if (interactable)
        {
            // Initializing the hours values
            hoursMinValue = 1;
            hoursMaxValue = 12;
            hoursCurrentValue = Random.Range(hoursMinValue, hoursMaxValue);

            // Initializing the minutes values
            minutesMinValue = 0;
            minutesMaxValue = 59;
            minutesCurrentValue = Random.Range(minutesMinValue, minutesMaxValue);

            // Handle renderer components
            hoursRenderer = hoursObj.GetComponentInChildren<Renderer>();
            minutesRenderer = minutesObj.GetComponentInChildren<Renderer>();

            inactive = (int)HandleRenderer.inactive;
            active = (int)HandleRenderer.active;

            // Handle text components
            hoursValueText = hoursValueTextObj.GetComponent<Text>();
            minutesValueText = minutesValueTextObj.GetComponent<Text>();

            player = GameObject.FindGameObjectWithTag("Player");

            if (valueChangeRate > maxValueChangeRate)
            {
                valueChangeRate = maxValueChangeRate;
            }
            originalValueChangeRate = valueChangeRate;

            SetClockTime();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (toggled)
        {
            HandleValueUp();
            HandleValueDown();
            ChangeHandle();
            CheckExitInteraction();
            ResetValueChangeRate();
        }
    }

    public void EnterInteraction()
    {
        toggled = true;

        hoursValueTextObj.SetActive(true);
        minutesValueTextObj.SetActive(true);
        clockCamObj.SetActive(true);
        player.SetActive(false);

        // Set the material of the active handle as the active material
        if (currentHandle == CurrentHandle.hours)
            hoursRenderer.material = materials[active];
        else
            minutesRenderer.material = materials[active];

        InstructionsTextObj.SetActive(true);
    }

    #region Private Methods
    private void ExitInteraction()
    {
        toggled = false;

        hoursValueTextObj.SetActive(false);
        minutesValueTextObj.SetActive(false);
        clockCamObj.SetActive(false);
        player.SetActive(true);

        // Set the material of both handles on the inactive one
        hoursRenderer.material = materials[inactive];
        minutesRenderer.material = materials[inactive];

        InstructionsTextObj.SetActive(false);

        EnterTime();
    }

    private void EnterTime()
    {
        if (hoursCurrentValue == hoursDesiredValue)
        {
            hoursCorrect = true;
            LoggingManager.Instance.AddToDictionary("EnteredCorrectHoursTime");
        }

        if (minutesCurrentValue == minutesDesiredValue)
        {
            minutesCorrect = true;
            LoggingManager.Instance.AddToDictionary("EnteredCorrectMinutesTime");
        }

        if (hoursCorrect && minutesCorrect)
        {
            timeCorrect = true;
            LoggingManager.Instance.AddToDictionary("EnteredCorrectTime");
        }

        LoggingManager.Instance.AddToDictionary("EnteredTime");
    }

    private void ChangeHandle()
    {
        // S Key = DOWN
        if (Input.GetKeyDown(KeyCode.S))
        {
            // HOURS --> MINUTES
            if (currentHandle == CurrentHandle.hours)
            {
                currentHandle = CurrentHandle.minutes;
                hoursRenderer.material = materials[inactive];
                minutesRenderer.material = materials[active];
            }
            // MINUTES + ARROW DOWN --> HOURS
            else if (currentHandle == CurrentHandle.minutes)
            {
                currentHandle = CurrentHandle.hours;
                hoursRenderer.material = materials[active];
                minutesRenderer.material = materials[inactive];
            }
        }

        // W Key = UP
        if (Input.GetKeyDown(KeyCode.W))
        {
            // HOURS --> MINUTES
            if (currentHandle == CurrentHandle.hours)
            {
                currentHandle = CurrentHandle.minutes;
                hoursRenderer.material = materials[inactive];
                minutesRenderer.material = materials[active];
            }
            // MINUTES --> HOURS
            else if (currentHandle == CurrentHandle.minutes)
            {
                currentHandle = CurrentHandle.hours;
                hoursRenderer.material = materials[active];
                minutesRenderer.material = materials[inactive];
            }
        }
    }

    private void HandleValueUp()
    {
        // Increase value of hours
        if (Input.GetKey(KeyCode.D) && currentHandle == CurrentHandle.hours && CanChangeValue)
        {
            SetNextValueChangeAndSpeedUp();

            // Loop back to min value if current gets higher than max value
            if (hoursCurrentValue == hoursMaxValue)
            {
                hoursCurrentValue = hoursMinValue;
            }
            else
                hoursCurrentValue++;

            SetClockTime();

            LoggingManager.Instance.AddToDictionary("HoursValueChanged");
            LoggingManager.Instance.AddToDictionary("HoursValueChangedUp");
        }
        // Increase value of minutes
        else if (Input.GetKey(KeyCode.D) && currentHandle == CurrentHandle.minutes && CanChangeValue)
        {
            SetNextValueChangeAndSpeedUp();

            // Loop back to min value if current gets higher than max value
            if (minutesCurrentValue == minutesMaxValue)
            {
                minutesCurrentValue = minutesMinValue;

                // Recheck this to make sure the hours have the right value
                if (hoursCurrentValue == hoursMaxValue)
                {
                    hoursCurrentValue = hoursMinValue;
                }
                else
                    hoursCurrentValue++;
            }
            else
                minutesCurrentValue++;

            SetClockTime();

            LoggingManager.Instance.AddToDictionary("MinutesValueChanged");
            LoggingManager.Instance.AddToDictionary("MinutesValueChangedUp");
        }
    }

    private void HandleValueDown()
    {
        // Decrease value of hours
        if (Input.GetKey(KeyCode.A) && currentHandle == CurrentHandle.hours && CanChangeValue)
        {
            SetNextValueChangeAndSpeedUp();

            // Loop back to max value if current gets lower than min value
            if (hoursCurrentValue == hoursMinValue)
            {
                hoursCurrentValue = hoursMaxValue;
            }
            else
                hoursCurrentValue--;

            SetClockTime();

            LoggingManager.Instance.AddToDictionary("HoursValueChanged");
            LoggingManager.Instance.AddToDictionary("HoursValueChangedDown");
        }
        // Decrease value of minutes
        else if (Input.GetKey(KeyCode.A) && currentHandle == CurrentHandle.minutes && CanChangeValue)
        {
            SetNextValueChangeAndSpeedUp();

            // Loop back to max value if current gets lower than min value
            if (minutesCurrentValue == minutesMinValue)
            {
                minutesCurrentValue = minutesMaxValue;

                // Recheck this to make sure the hours have the right value
                if (hoursCurrentValue == hoursMinValue)
                {
                    hoursCurrentValue = hoursMaxValue;
                }
                else
                    hoursCurrentValue--;
            }
            else
                minutesCurrentValue--;

            SetClockTime();

            LoggingManager.Instance.AddToDictionary("MinutesValueChanged");
            LoggingManager.Instance.AddToDictionary("MinutesValueChangedDown");
        }
    }

    private void SetNextValueChangeAndSpeedUp()
    {
        // Set the time for next value change
        nextTimeToChange = Time.time + (1f / valueChangeRate);

        if (valueChangeRate < maxValueChangeRate)
        {
            valueChangeRate += addToValueChangeRate; 
        }
    }

    private void ResetValueChangeRate()
    {
        if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
        {
            valueChangeRate = originalValueChangeRate; 
        }
    }

    private void CheckExitInteraction()
    {
        if (Input.GetKeyDown(KeyCode.Q))
            ExitInteraction();
    }

    private void SetHours(int hour, int minute)
    {
        // hour * 30:   every hour is 30deg from the other, so that times hour gets us the right one.
        // minute / 2:  an hour is 30deg and has 60 minutes, so for 60 minutes we want to move a max of 30deg
        //              so if we devide minute by 2 we get the middle. This way the hours handle rotates according
        //              to the minutes handle.
        hoursObj.transform.localRotation = Quaternion.Euler(90, 0, (hour * 30) + (minute / 2));
        hoursValueText.text = "Hours: " + hoursCurrentValue;
    }

    private void SetMinutes(int minute)
    {
        // minute * 30 / 5: every hour is on a 5 minute interval, this means we can devide that by 5 and get 1 minute.
        minutesObj.transform.localRotation = Quaternion.Euler(90, 0, ((minute * 30) / 5));
        minutesValueText.text = "Minutes: " + minutesCurrentValue;
    }

    private void SetClockTime()
    {
        SetHours(hoursCurrentValue, minutesCurrentValue);
        SetMinutes(minutesCurrentValue);
        LoggingManager.Instance.AddToDictionary("ClockSet");
    } 
    #endregion

    #region Properties
    public bool HoursCorrect
    {
        get { return hoursCorrect; }
    }

    public bool MinutesCorrect
    {
        get { return minutesCorrect; }
    }

    public bool TimeCorrect
    {
        get { return timeCorrect; }
    }

    private bool CanChangeValue
    {
        get
        {
            if (Time.time > nextTimeToChange)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    #endregion
}

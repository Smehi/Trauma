﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBehavior : MonoBehaviour {
    public enum OpenDirection { inwards = -1, outwards = 1}
    public Vector3 hingPoint;
    public Vector3 rotateAroundPoint;
    public Vector3 axis = Vector3.up;
    public Vector3 outAxis;
    private AudioSource audioSource;

    [Range(0,360)]public float openAngle;
    [Range(0,360)] public float angleSpeed;
    public OpenDirection openDirection = OpenDirection.inwards;
    private float angleProgres;
    private float curAngle;
    private float goalAngle;
    private bool doorRotating = false;
    private bool opening = false;

    private Quaternion originalRotation;
    private Vector3 originalPosition;

    public List<AudioClip> audioFilesToPlayOnDoorActivate = new List<AudioClip>();
    public bool isDoorOpenable;
    public bool activateDoor;

    public bool movesBack = true;

	// Use this for initialization
	void Start () {
        originalRotation = this.transform.rotation;
        this.originalPosition = this.transform.position;
        this.rotateAroundPoint = this.transform.position - hingPoint;
        this.audioSource = this.transform.GetComponent<AudioSource>();
        openAngle *= (int)openDirection;
        angleSpeed *= (int)openDirection;

        if(movesBack)
            MoveBackManager.onCollideBox += StartCopy;
    }

    void StartCopy()
    {
        originalRotation = this.transform.rotation;
        this.originalPosition = this.transform.position;
        this.rotateAroundPoint = this.transform.position - hingPoint;
        MoveBackManager.onCollideBox -= StartCopy;//unsub
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        Debug.DrawLine(this.transform.position, this.rotateAroundPoint, Color.red, 2f);
        if (activateDoor)
        {
            this.ActivateDoor();
        }
        if (doorRotating)
        {
            var direction = opening ? -1 : 1;
            this.transform.RotateAround(rotateAroundPoint, axis, direction * angleSpeed * Time.fixedDeltaTime);
            this.transform.localRotation.ToAngleAxis(out curAngle, out outAxis);
            direction *= (int)openDirection;
            if (CalcAngleDif(goalAngle, curAngle * direction) <= Mathf.Abs(angleSpeed * Time.fixedDeltaTime))
            {
                doorRotating = false;
                if (!this.opening)
                {
                    this.transform.rotation = this.originalRotation;
                    this.transform.position = this.originalPosition;
                }
            }
            
        }
	}

    public void ActivateDoor()
    {
        this.activateDoor = false;
        if (this.audioFilesToPlayOnDoorActivate.Count != 0)
        {
            this.audioSource.clip = this.GetRandomSoundFile(this.audioFilesToPlayOnDoorActivate);
            this.audioSource.Play();
        }
        if (isDoorOpenable)
        {
            doorRotating = true;
            opening = !opening;
            if (opening)
            {
                goalAngle = 360 - this.openAngle;
            }
            else
            {
                goalAngle = 0;
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        CanOpenDoor doorDude = getDoorScript(other.transform);
        if (doorDude != null)
            doorDude.doorList.Add(this);

    }

    public void OnTriggerExit(Collider other)
    {
        CanOpenDoor doorDude = getDoorScript(other.transform);
        if(doorDude != null)
            doorDude.doorList.Remove(this);
    }

    private CanOpenDoor getDoorScript(Transform other)
    {
        var doorDude = other.GetComponent<CanOpenDoor>();
        if (doorDude == null)
        {
            doorDude = other.GetComponentInChildren<CanOpenDoor>();
            if (doorDude == null)
            {
                doorDude = other.GetComponentInParent<CanOpenDoor>();
                if (doorDude)
                {
                    return null;//apperently the dude can't open doors.
                }
            }
        }
        return doorDude;
    }

    public float CalcAngleDif(float angle1, float angle2)
    {
        var r = Mathf.Abs(angle1 - angle2);
        while (r > 360)
        {
            r -= 360;
        }
        return r;
    }

    public AudioClip GetRandomSoundFile(List<AudioClip> list)
    {
        int index = Random.Range(0, list.Count);
        return list[index];
    }
}

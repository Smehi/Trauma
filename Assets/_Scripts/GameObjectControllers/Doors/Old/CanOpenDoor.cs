﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanOpenDoor : MonoBehaviour {

    public List<DoorBehavior> doorList = new List<DoorBehavior>();
    bool textOnScreen = false;
    public KeyCode doorKey;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(doorList.Count != 0)
        {
            if (Input.GetKeyDown(doorKey))
            {
                doorList[0].ActivateDoor();
            }
        }
	}
}

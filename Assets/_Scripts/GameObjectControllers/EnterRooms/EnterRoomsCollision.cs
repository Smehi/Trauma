﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterRoomsCollision : MonoBehaviour
{
    EnterRoomsManager enterRooms;

    // Use this for initialization
    void Start()
    {
        enterRooms = GetComponentInParent<EnterRoomsManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            enterRooms.RoomEntered(this.gameObject);
    }
}

﻿using System.Collections;
using System.Text;
using System.Collections.Generic;
using UnityEngine;

public class EnterRoomsManager : MonoBehaviour
{
    public enum Rooms { restroom, kitchen, livingroom }

    private StringBuilder errorOutput = new StringBuilder();
    private List<string> errorList = new List<string>();

    [SerializeField] private List<Rooms> correctList = new List<Rooms>();
    [SerializeField] private List<Rooms> playerList = new List<Rooms>();

    // Use this for initialization
    void Start()
    {
        // In case someone add anything to the playerList in the inspector we clear it here.
        // The reason it is visable in the first place is for debugging purposes.
        playerList.Clear();

        #region Checking for impossible room enter orders
        // This check for the same room that is back to back and also tells us which two Elements.
        for (int i = 0; i < correctList.Count - 1; i++)
        {
            if (correctList[i + 1] == correctList[i])
            {
                errorList.Add(string.Format("WARNING! Two of the same room are back to back --- Element {0,1} & Element {1,1}\n", i, (i + 1)));
            }
        }
        #endregion

        if (errorList.Count > 0)
        {
            foreach (string error in errorList)
            {
                errorOutput.Append(error);
            }

            Debug.LogWarning(errorOutput.ToString()); 
        }
    }

    public void RoomEntered(GameObject child)
    {
        switch (child.name)
        {
            case "Restroom":
                playerList.Add(Rooms.restroom);
                break;
            case "Kitchen":
                playerList.Add(Rooms.kitchen);
                break;
            case "Livingroom":
                playerList.Add(Rooms.livingroom);
                break;
            default:
                break;
        }
    }

    public bool CheckIfPlayerListEqualsCorrectList
    {
        get
        {
            if (playerList.Count >= correctList.Count)
            {
                int correcListIndex = 0;
                // i = playerList.Count - correctList.Count: because then we start checking for the amount of rooms in the correctList
                for (int i = playerList.Count - correctList.Count; i < playerList.Count; i++)
                {
                    if (playerList[i] != correctList[correcListIndex])
                    {
                        return false; // Return false if playerList order is not the same as correctList
                    }

                    correcListIndex++;
                }
            }
            else
            {
                return false;
            }

            return true; // Return true if playerList order is the same as correctList
        }
    }

    [ContextMenu("Check if the rooms are in the correct order.")]
    private void CheckIfPlayerListEqualsCorrectListDebug()
    {
        if (CheckIfPlayerListEqualsCorrectList)
        {
            Debug.Log("The rooms are in the correct order!");
        }
        else
        {
            Debug.Log("The rooms are in the wrong order!");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyLookAtPlayer : MonoBehaviour
{
    [Header("Animation reverse time")]
    public float minAnimationReverseTime;
    public float maxAnimationReverseTime;

    [Header("Animation speed")]
    public float minAnimationSpeed;
    public float maxAnimationSpeed;

    [Header("Sound settings")]
    public AudioClip[] audioOnDisappear;

    private GameObject player;
    private Animator animator;
    private GameObject audioSourceObj;
    private AudioSource audioSource;
    private CheckIfObjectInView checkIfObjectInView;

    [Header("Debugging")]
    [SerializeField]
    private float timeUntilReverseAnimation;
    [SerializeField]
    private float randomAnimationSpeed;
    [SerializeField]
    private bool activated = false;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        animator = GetComponent<Animator>();
        checkIfObjectInView = GetComponent<CheckIfObjectInView>();

        animator.SetFloat("IdleSpeed", 1f);

        timeUntilReverseAnimation = GetRandomTime();
        randomAnimationSpeed = GetRandomAnimationSpeed();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RotateTowardsPlayer();

        if (checkIfObjectInView.TeddyHasBeenSeen && !activated)
        {
            TeleportBehindPlayer();
            checkIfObjectInView.TeddyHasBeenSeen = false;
        }
        else if (checkIfObjectInView.TeddyHasBeenSeen && activated)
        {
            DisappearAfterTeleport();
        }

        timeUntilReverseAnimation -= Time.fixedDeltaTime;

        if (timeUntilReverseAnimation <= 0)
        {
            if (randomAnimationSpeed > 0)
            {
                randomAnimationSpeed = GetRandomAnimationSpeed() * -1;
            }
            else
            {
                randomAnimationSpeed = GetRandomAnimationSpeed();
            }

            animator.SetFloat("IdleSpeed", randomAnimationSpeed);

            GetRandomTime();
        }
    }

    private void TeleportBehindPlayer()
    {
        activated = true;
        transform.position = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z) + (Vector3.forward * 1.5f);
        Debug.Log("Teddy position: " + transform.position);
        audioSourceObj = new GameObject("Audiosource");
        audioSourceObj.transform.position = transform.position;
        audioSource = audioSourceObj.AddComponent<AudioSource>();
        audioSource.spatialBlend = 1;
    }

    private void DisappearAfterTeleport()
    {
        int randomIndex = Random.Range(0, audioOnDisappear.Length);
        audioSource.PlayOneShot(audioOnDisappear[randomIndex]);
        Destroy(gameObject, audioOnDisappear[randomIndex].length);
        Destroy(audioSourceObj, audioOnDisappear[randomIndex].length);
    }

    private void RotateTowardsPlayer()
    {
        transform.LookAt(player.transform.position);
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
    }

    private float GetRandomTime()
    {
        return timeUntilReverseAnimation = Random.Range(minAnimationReverseTime, maxAnimationReverseTime);
    }

    private float GetRandomAnimationSpeed()
    {
        return randomAnimationSpeed = Random.Range(minAnimationSpeed, maxAnimationSpeed);
    }
}

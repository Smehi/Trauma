﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckIfObjectInView : MonoBehaviour
{
    public float raycastRange;

    private GameObject targetObj;
    private TeddyRunOnTrigger teddyRunScript;
    private Collider myCollider;
    private RaycastHit hit;
    private Camera cam;
    private Plane[] planes;
    private bool teddyHasBeenSeen;

    // Use this for initialization
    void Start()
    {
        targetObj = GameObject.FindGameObjectWithTag("Player");
        teddyHasBeenSeen = false;
        cam = Camera.main;
        myCollider = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        planes = GeometryUtility.CalculateFrustumPlanes(cam);
        Debug.DrawRay(transform.position, (targetObj.transform.position - transform.position).normalized * raycastRange, Color.red);
        if (GeometryUtility.TestPlanesAABB(planes, myCollider.bounds))//teddy is view box
        {
            if (Physics.Raycast(transform.position, (targetObj.transform.position - transform.position).normalized, out hit, raycastRange) && hit.collider.gameObject.tag == "Player")
            {
                teddyHasBeenSeen = true;
            }
        }
    }

    public RaycastHit RaycastHit
    {
        get { return hit; }
    }

    public bool TeddyHasBeenSeen
    {
        get { return teddyHasBeenSeen; }
        set { teddyHasBeenSeen = value; }
    }
}

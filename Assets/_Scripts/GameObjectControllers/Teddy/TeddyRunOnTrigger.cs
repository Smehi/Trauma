﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeddyRunOnTrigger : MonoBehaviour
{
    [Range (1f, 3f)] public float speed;
    public GameObject player;
    public AudioClip audioOnRun;

    private bool activated = false;

    Vector3 vec;
    CheckIfObjectInView checkIfObjectInView;

    Animator anim;
    AudioSource AS;

    // Use this for initialization
    void Start()
    {
        checkIfObjectInView = GetComponent<CheckIfObjectInView>();
        anim = GetComponent<Animator>();
        AS = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (checkIfObjectInView.TeddyHasBeenSeen && !activated)
            Activate();
    }

    void Activate()
    {
        anim.SetFloat("Speed", speed);
        transform.Translate(Vector3.forward * speed * Time.fixedDeltaTime);
        AS.PlayOneShot(audioOnRun);
        activated = !activated;
    }
}
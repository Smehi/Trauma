﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetScript : MonoBehaviour {

    public bool setPos;// If false it will add the pos values to the current position
    public float posX, posY, posZ;

    public bool setRot; // If false it will add the rot values to the current rotation
    public float rotX, rotY, rotZ;
    
	// Use this for initialization
	void Start() {
        MoveBackManager.onCollideBox += Reset; // Add the Reset method as a subscriber; This gets called when collision happens with Player and MoveBackCollider
	}

    void Reset()
    {
        if (setPos && gameObject.tag != "Player")
        {
            Vector3 pos = new Vector3(posX, posY, posZ);
            this.transform.position = pos;
        }
        else if (setPos && gameObject.tag == "Player")
        {
            var zOffset = transform.position.x - 0.425f;
            Vector3 pos = new Vector3(posX, posY, posZ + zOffset);
            this.transform.position = pos;
        }
        else
        {
            Vector3 pos = new Vector3(transform.position.x + posX, transform.position.y + posY, transform.position.z + posZ);
            this.transform.position = pos;
        }

        if (setRot)
        {
            Quaternion rot = Quaternion.Euler(rotX, rotY, rotZ);
            this.transform.rotation = rot;
            if (this.gameObject.tag == "Player")//player hack
            {
                //this.gameObject.GetComponentInChildren<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().m_MouseLook.SetCharacterRotation();
            }
        }
        else
        {
            Quaternion rot = Quaternion.Euler(transform.rotation.x + rotX, transform.rotation.y + rotY, transform.rotation.z + rotZ);
            this.transform.rotation = rot;
            if(this.gameObject.tag == "Player")//player hack
            {
                var firstPersonController = this.gameObject.GetComponentInChildren<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
                firstPersonController.m_MouseLook.RotateCharacter(transform, firstPersonController.m_Camera.transform, new Vector2(rotX, rotY));
            }
        }

        if (this.gameObject.tag != "Player")
        {
            MoveBackManager.onCollideBox -= Reset;
        }     

        MoveBackManager.Instance.Collided = false; // Turn this back to false so collision with the box is possible again
    }
}

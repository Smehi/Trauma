﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SjorsGielen.Chessboard
{

    [DisallowMultipleComponent][RequireComponent(typeof(AudioSource))]
    public class ChessboardStateDrawer : MonoBehaviour
    {
        public enum BoardType
        {
            Marble, Metallic, Plastic
        }
        [SerializeField][Tooltip("Which prefabs should be loaded in. Changes the actual models loaded in! Does not change the board during runtime, for that call the boardType property")]
        private BoardType boardType = BoardType.Marble;

        public BoardType _boardType
        {
            set
            {
                this.boardType = value;
                LoadPiecesToGameobjects();
                DrawNewBoardState();
            }
            get { return this.boardType; }
        }

        [Header("Interact variables")]
        public bool isInteractable = true;
        public GameObject myInteractCamera;
        public ChessboardContoller myChessboardController;
        public GameObject player;

        public float chesspieceOfssetHeight = 2f;

        private ChessPiece[,] chessboard = new ChessPiece[8, 8];// a chessboard is 8x8

        public ChessPiece[,] _chessboard
        {
            get { return (ChessPiece[,])this.chessboard.Clone(); }
            set {
                this.chessboard = value;
                DrawNewBoardState();
                }
        }

        Dictionary<ChessPiece, GameObject> piecesToObjects;

        AudioSource soundEmmiter;
        public List<AudioClip> audioFilesToPlayOnChesspieceMove = new List<AudioClip>();

        [Header("If puzzle component is attached it should be shown below")]
        public ChessboardPuzzle myPuzzleComponent;

        [Header("A debug list of pieces")]
        public List<ChessPiece> debugListOfPieces = new List<ChessPiece>
        {
            ChessPiece.BlackBishop, ChessPiece.BlackKnight, ChessPiece.BlackKnight,
            ChessPiece.WhiteRook, ChessPiece.WhitePawn, ChessPiece.WhitePawn,
            ChessPiece.WhitePawn, ChessPiece.WhitePawn, ChessPiece.WhitePawn,
            ChessPiece.WhiteKing, ChessPiece.BlackKing
        };
        

        [Header("Some debug stuff the game will set")]
        public List<GameObject> activePieces = new List<GameObject>();

        // Use this for initialization
        void Start()
        {
            if(isInteractable)
            {
                myInteractCamera = Instantiate(Resources.Load("Chess/Prefabs/BoardCameraPrefab") as GameObject, this.transform);
                myChessboardController = this.gameObject.AddComponent<ChessboardContoller>();
                myInteractCamera.SetActive(false);
                myChessboardController.enabled = false;
                player = GameObject.FindGameObjectWithTag("Player");
            }
            soundEmmiter = GetComponent<AudioSource>();
            myPuzzleComponent = GetComponent<ChessboardPuzzle>();
            LoadPiecesToGameobjects();
            
            if(myPuzzleComponent == null)
            {
                this._chessboard = ChessboardStates.DefaultChessboardState();
            }
            else
            {
                this._chessboard = myPuzzleComponent.GetStartState(this);
            }
        }

        public void ToggleInteractableState()
        {
            if(myChessboardController.enabled)
            {
                myChessboardController.GoingToBeDisabled();
            }
            myInteractCamera.SetActive(!myInteractCamera.activeSelf);
            myChessboardController.enabled = !myChessboardController.enabled;
            player.SetActive(!player.activeSelf);
            if(myChessboardController.enabled)
            {
                myChessboardController.JustEnabled();
            }
        }

        [ContextMenu("Debug boardstate test")]
        public void DebugBoardstate()
        {
            chessboard = RandomizeBoardState(debugListOfPieces);
            DrawNewBoardState();
        }

        /// <summary>
        /// Loads in the assets from resources folder for the chesspieces NOTE: happens automaticly if the _boardType is set
        /// </summary>
        private void LoadPiecesToGameobjects()
        {
            this.piecesToObjects = new Dictionary<ChessPiece, GameObject>();
            switch (boardType)
            {
                case BoardType.Marble:
                    piecesToObjects.Add(ChessPiece.BlackBishop, Resources.Load("Chess/Prefabs/Marble/Black Bishop") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackKing, Resources.Load("Chess/Prefabs/Marble/Black King") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackKnight, Resources.Load("Chess/Prefabs/Marble/Black Knight") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackPawn, Resources.Load("Chess/Prefabs/Marble/Black Pawn") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackQueen, Resources.Load("Chess/Prefabs/Marble/Black Queen") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackRook, Resources.Load("Chess/Prefabs/Marble/Black Rook") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteBishop, Resources.Load("Chess/Prefabs/Marble/White Bishop") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteKing, Resources.Load("Chess/Prefabs/Marble/White King") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteKnight, Resources.Load("Chess/Prefabs/Marble/White Knight") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhitePawn, Resources.Load("Chess/Prefabs/Marble/White Pawn") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteQueen, Resources.Load("Chess/Prefabs/Marble/White Queen") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteRook, Resources.Load("Chess/Prefabs/Marble/White Rook") as GameObject);
                    break;
                case BoardType.Metallic:
                    piecesToObjects.Add(ChessPiece.BlackBishop, Resources.Load("Chess/Prefabs/Metallic/Black Bishop") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackKing, Resources.Load("Chess/Prefabs/Metallic/Black King") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackKnight, Resources.Load("Chess/Prefabs/Metallic/Black Knight") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackPawn, Resources.Load("Chess/Prefabs/Metallic/Black Pawn") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackQueen, Resources.Load("Chess/Prefabs/Metallic/Black Queen") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackRook, Resources.Load("Chess/Prefabs/Metallic/Black Rook") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteBishop, Resources.Load("Chess/Prefabs/Metallic/White Bishop") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteKing, Resources.Load("Chess/Prefabs/Metallic/White King") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteKnight, Resources.Load("Chess/Prefabs/Metallic/White Knight") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhitePawn, Resources.Load("Chess/Prefabs/Metallic/White Pawn") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteQueen, Resources.Load("Chess/Prefabs/Metallic/White Queen") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteRook, Resources.Load("Chess/Prefabs/Metallic/White Rook") as GameObject);
                    break;
                case BoardType.Plastic:
                    piecesToObjects.Add(ChessPiece.BlackBishop, Resources.Load("Chess/Prefabs/Plastic/Black Bishop") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackKing, Resources.Load("Chess/Prefabs/Plastic/Black King") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackKnight, Resources.Load("Chess/Prefabs/Plastic/Black Knight") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackPawn, Resources.Load("Chess/Prefabs/Plastic/Black Pawn") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackQueen, Resources.Load("Chess/Prefabs/Plastic/Black Queen") as GameObject);
                    piecesToObjects.Add(ChessPiece.BlackRook, Resources.Load("Chess/Prefabs/Plastic/Black Rook") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteBishop, Resources.Load("Chess/Prefabs/Plastic/White Bishop") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteKing, Resources.Load("Chess/Prefabs/Plastic/White King") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteKnight, Resources.Load("Chess/Prefabs/Plastic/White Knight") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhitePawn, Resources.Load("Chess/Prefabs/Plastic/White Pawn") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteQueen, Resources.Load("Chess/Prefabs/Plastic/White Queen") as GameObject);
                    piecesToObjects.Add(ChessPiece.WhiteRook, Resources.Load("Chess/Prefabs/Plastic/White Rook") as GameObject);
                    break;
                default:
                    Debug.LogWarning("Unexpected type in chessboardtype");
                    break;
            }
            foreach(KeyValuePair<ChessPiece, GameObject> p in piecesToObjects)
            {
                if (p.Key >= ChessPiece.BlackBishop && p.Key <= ChessPiece.BlackRook)
                    p.Value.transform.localRotation = new Quaternion(0, 1, 0, 0);
            }
            Debug.Log("Loaded chesspiece objects of type " + boardType.ToString());
        }


        /// <summary>
        /// Clears out any existing chesspieces and instantiates the chesspieces for the current state the board is in.
        /// </summary>
        public void DrawNewBoardState()
        {
            foreach(GameObject piece in activePieces)
            {
                Destroy(piece);
            }
            activePieces.Clear();
            var xlenght = chessboard.GetLength(0);
            var ylength = chessboard.GetLength(1);
            for (int x = 0; x < xlenght; x++)
            {
                for (int y = 0; y < ylength; y++)
                {
                    if(chessboard[x,y] != ChessPiece.None)
                    {
                        var newPiece = Instantiate(piecesToObjects[chessboard[x, y]], this.transform);
                        newPiece.transform.localPosition += this.PointToVector3(new Point(x, y)) + new Vector3(0, chesspieceOfssetHeight, 0);
                        activePieces.Add(newPiece);
                    }
                }
            }
        }

        /// <summary>
        /// Places pieces randomly across the board.
        /// tries to place the pieces from the list randomly
        /// </summary>
        /// <param name="piecesToPlace">List of pieces to place</param>
        /// 
        public ChessPiece[,] RandomizeBoardState(List<ChessPiece> piecesToPlace)
        {
            var newChessboard = new ChessPiece[chessboard.GetLength(0), chessboard.GetLength(1)];//clear boardstate

            foreach(ChessPiece p in piecesToPlace)
            {
                Point spot = new Point(0,0);
                int attempts = 0;
                do
                {
                    attempts++;
                    if (attempts > 50)
                    {
                        Debug.LogWarning("Chessboard spot placement has cost more then 50 attempts, skipping");
                        break;
                    }
                    spot = GetRandomPointOnChessboard(newChessboard.GetLength(0), newChessboard.GetLength(1));
                } while (IsPossitionFilled(spot, newChessboard));
                newChessboard[spot.x, spot.y] = p;
            }
            return newChessboard;
        }

        /// <summary>
        /// Returns a random point between params arguments
        /// </summary>
        /// <param name="lowerX">Lower var for the x var in the point [inclusive]</param>
        /// <param name="upperX">Upper var for the x var in the point [exclusive]</param>
        /// <param name="lowerY">Lower var for the y var in the point [inclusive]</param>
        /// <param name="upperY">Upper var for the y var in the point [exclusive]</param>
        /// <returns>A random point within the two bounds</returns>
        private Point GetRandomPointOnChessboard(int lowerX, int upperX, int lowerY, int upperY)
        {
            return new Point(Random.Range(lowerX, upperX), Random.Range(lowerY, upperY));//maybe have the dimensions come in through param?
        }

        /// <summary>
        /// Returns a random point between 0 and params arguments
        /// </summary>
        /// <param name="upperX">Upper var for the x var in the point [exclusive]</param>
        /// <param name="upperY">Upper var for the y var in the point [exclusive]</param>
        /// <returns>A random point within the two bounds</returns>
        private Point GetRandomPointOnChessboard(int upperX, int upperY)
        {
            return new Point(Random.Range(0, upperX), Random.Range(0, upperY));//maybe have the dimensions come in through param?
        }

        /// <summary>
        /// Checks if the possition argument is filled in the chessboard arugment
        /// </summary>
        /// <param name="possitionToCheck">The possition to check</param>
        /// <param name="boardState">The boardstate to check on</param>
        /// <returns>Returns true if the possition is filled</returns>
        private bool IsPossitionFilled(Point possitionToCheck, ChessPiece[,] boardState)
        {
            if(boardState[possitionToCheck.x, possitionToCheck.y] == ChessPiece.None)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Swaps two chesspieces on the board and returns a new instance of a chessboard
        /// </summary>
        /// <param name="p1">The first point on the board</param>
        /// <param name="p2">The second point on the board</param>
        /// <returns>A copy of the current chessboard with the two points swapped</returns>
        public ChessPiece[,] SwapPossitions(Point p1, Point p2)
        {
            LoggingManager.Instance.AddToDictionary("SwappedSpotsOnBoard" + this);
            soundEmmiter.PlayOneShot(audioFilesToPlayOnChesspieceMove[Random.Range(0, audioFilesToPlayOnChesspieceMove.Count)]);
            LoggingManager.Instance.AddToDictionary("Swapping: " + this.chessboard[p1.x, p1.y] + " " + p1.x + " " + p1.y + " With " + this.chessboard[p2.x, p2.y] + " " + p2.x + " " + p2.y);
            ChessPiece[,] newBoardState = (ChessPiece[,])this.chessboard.Clone();
            newBoardState[p1.x, p1.y] = this.chessboard[p2.x, p2.y];
            newBoardState[p2.x, p2.y] = this.chessboard[p1.x, p1.y];
            return newBoardState;
        }

        public Vector3 PointToVector3(Point point)
        {
            return new Vector3(32 - point.x * 64 / 7, 6f, 32 - point.y * 64 / 7);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace SjorsGielen.Chessboard
{
    public class ChessboardContoller : MonoBehaviour
    {

        public ChessboardStateDrawer chessboardDrawerScript;

        public GameObject boardCanvas;
        public GameObject selectorBox;
        public GameObject selectedBox;

        public Point cursorPossition = new Point(4, 4);
        public Point currentSelectorPossition = new Point(4, 4);

        public bool selectedSomething = false;
        public Point selectedPossition = new Point(-1, -1);

        public ChessPiece chessPieceSelected = ChessPiece.None;
        [SerializeField]
        private Vector2 prevInput;
        [SerializeField]
        private Vector2 input;
        [SerializeField]
        private bool lockedAxisX = false;
        [SerializeField]
        private bool lockedAxisY = false;

        public Vector2 debugvector;

        // Use this for initialization
        void Start()
        {
            chessboardDrawerScript = GetComponent<ChessboardStateDrawer>();
            if (selectorBox == null && selectedBox == null)
            {
                boardCanvas = Instantiate(Resources.Load("Chess/Pregabs/BoardCanvas") as GameObject, this.transform);
                selectorBox = Instantiate(Resources.Load("Chess/Prefabs/Selector") as GameObject, this.transform);
                selectedBox = Instantiate(Resources.Load("Chess/Prefabs/Selected") as GameObject, this.transform);
                boardCanvas.SetActive(false);
                selectorBox.SetActive(false);
                selectedBox.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if (!lockedAxisX)
            {
                if (Mathf.Abs(input.x) > 0.8f)
                {
                    lockedAxisX = true;
                    cursorPossition.x -= Mathf.RoundToInt(input.x);
                }
            }
            else
            {
                if (Mathf.Abs(input.x) < 0.8f)
                {
                    lockedAxisX = false;
                }
            }
            if (!lockedAxisY)
            {
                if (Mathf.Abs(input.y) > 0.8f)
                {
                    lockedAxisY = true;
                    cursorPossition.y -= Mathf.RoundToInt(input.y);
                }
            }
            else
            {
                if (Mathf.Abs(input.y) < 0.8f)
                {
                    lockedAxisY = false;
                }
            }

            if (cursorPossition.x >= chessboardDrawerScript._chessboard.GetLength(0))
            {
                cursorPossition.x -= chessboardDrawerScript._chessboard.GetLength(0);
            }
            if (cursorPossition.x < 0)
            {
                cursorPossition.x += chessboardDrawerScript._chessboard.GetLength(0);
            }

            if (cursorPossition.y >= chessboardDrawerScript._chessboard.GetLength(1))
            {
                cursorPossition.y -= chessboardDrawerScript._chessboard.GetLength(1);
            }
            if (cursorPossition.y < 0)
            {
                cursorPossition.y += chessboardDrawerScript._chessboard.GetLength(1);
            }

            debugvector = new Vector2(cursorPossition.x, cursorPossition.y);
            selectorBox.transform.localPosition = PointToVector3(cursorPossition);
            if (selectedSomething)
            {
                //Something is selected, picking the second possition
                if (Input.GetButtonDown("Interact button"))
                {
                    chessboardDrawerScript._chessboard = chessboardDrawerScript.SwapPossitions(selectedPossition, cursorPossition);
                    selectedSomething = false;
                    if (chessboardDrawerScript.myPuzzleComponent != null)
                    {
                        chessboardDrawerScript.myPuzzleComponent.EvaluateIfCompleted();
                    }
                    selectedBox.SetActive(false);
                }
            }
            else
            {
                //Nothing is selected, picking the first possition
                if (Input.GetButtonDown("Interact button"))
                {
                    if (chessboardDrawerScript._chessboard[cursorPossition.x, cursorPossition.y] != ChessPiece.None)
                    {
                        chessPieceSelected = chessboardDrawerScript._chessboard[cursorPossition.x, cursorPossition.y];
                        selectedBox.transform.localPosition = PointToVector3(cursorPossition) + new Vector3(0, .1f, 0);
                        selectedBox.SetActive(true);
                        selectedSomething = true;
                        selectedPossition = cursorPossition;
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                this.chessboardDrawerScript.ToggleInteractableState();
            }

            prevInput = input;
        }

        public void GoingToBeDisabled()
        {
            chessPieceSelected = ChessPiece.None;
            selectedSomething = false;
            boardCanvas.SetActive(false);
            selectorBox.SetActive(false);
            selectedBox.SetActive(false);
        }

        public void JustEnabled()
        {
            if (selectorBox == null && selectedBox == null)
            {
                boardCanvas = Instantiate(Resources.Load("Chess/Prefabs/BoardCanvas") as GameObject, this.transform);
                selectorBox = Instantiate(Resources.Load("Chess/Prefabs/Selector") as GameObject, this.transform);
                selectedBox = Instantiate(Resources.Load("Chess/Prefabs/Selected") as GameObject, this.transform);
            }

            boardCanvas.SetActive(true);
            selectorBox.SetActive(true);
            selectedBox.SetActive(false);
        }

        public Vector3 PointToVector3(Point point)
        {
            return new Vector3(32 - point.x * 64 / 7, 4f, 32 - point.y * 64 / 7);
        }
    }
}
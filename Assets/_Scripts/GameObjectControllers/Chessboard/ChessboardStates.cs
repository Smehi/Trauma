﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SjorsGielen.Chessboard
{
    /// <summary>
    /// A simple struct to keep coordinate int's toghther.
    /// </summary>
    public struct Point
    {
        public int x;
        public int y;

        public Point(int X, int Y)
        {
            x = X;
            y = Y;
        }
    }

    public enum ChessPiece
    {
        None,
        WhiteBishop, WhiteKing, WhiteKnight, WhitePawn, WhiteQueen, WhiteRook,
        BlackBishop, BlackKing, BlackKnight, BlackPawn, BlackQueen, BlackRook
    }

    public static class ChessboardStates
    {
        public static ChessPiece[,] DefaultChessboardState()
        {
            return new ChessPiece[,]  {
                { ChessPiece.WhiteRook, ChessPiece.WhiteKnight, ChessPiece.WhiteBishop, ChessPiece.WhiteKing, ChessPiece.WhiteQueen, ChessPiece.WhiteBishop, ChessPiece.WhiteKnight, ChessPiece.WhiteRook },
                { ChessPiece.WhitePawn, ChessPiece.WhitePawn, ChessPiece.WhitePawn, ChessPiece.WhitePawn, ChessPiece.WhitePawn, ChessPiece.WhitePawn, ChessPiece.WhitePawn, ChessPiece.WhitePawn },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackRook, ChessPiece.BlackKnight, ChessPiece.BlackBishop, ChessPiece.BlackKing, ChessPiece.BlackQueen, ChessPiece.BlackBishop, ChessPiece.BlackKnight, ChessPiece.BlackRook }
            };
        }

        public static ChessPiece[,] BishopPuzzleStartState()
        {
            return new ChessPiece[,]  {
                { ChessPiece.None, ChessPiece.None, ChessPiece.WhiteBishop, ChessPiece.WhiteBishop, ChessPiece.WhiteBishop, ChessPiece.WhiteBishop, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None },
                { ChessPiece.None, ChessPiece.None, ChessPiece.BlackBishop, ChessPiece.BlackBishop, ChessPiece.BlackBishop, ChessPiece.BlackBishop, ChessPiece.None, ChessPiece.None }
            };
        }

        public static ChessPiece[,] BishopPuzzleEndState()
        {
            return new ChessPiece[,]  {
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.BlackBishop, ChessPiece.BlackBishop, ChessPiece.BlackBishop, ChessPiece.BlackBishop, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.None, ChessPiece.BlackPawn, ChessPiece.BlackPawn },
                { ChessPiece.BlackPawn, ChessPiece.BlackPawn, ChessPiece.WhiteBishop, ChessPiece.WhiteBishop, ChessPiece.WhiteBishop, ChessPiece.WhiteBishop, ChessPiece.BlackPawn, ChessPiece.BlackPawn }
            };
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SjorsGielen.Chessboard
{
    public class ChessboardPuzzle : MonoBehaviour
    {
        public ChessboardStateDrawer myStateDrawer;
        public bool completed = false;

        void Start()
        {
            myStateDrawer = GetComponent<ChessboardStateDrawer>();
        }

        public virtual bool EvaluateIfCompleted()
        {
            return false;
        }

        public virtual ChessPiece[,] GetStartState(ChessboardStateDrawer stateDrawerFallback = null)
        {
            return new ChessPiece[8,8];
        }
    }
}

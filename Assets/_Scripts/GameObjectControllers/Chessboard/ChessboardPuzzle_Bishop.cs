﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SjorsGielen.Chessboard
{
    public class ChessboardPuzzle_Bishop : ChessboardPuzzle
    {

        public override ChessPiece[,] GetStartState(ChessboardStateDrawer stateDrawerFallback = null)
        {
            return ChessboardStates.BishopPuzzleStartState();
        }

        public override bool EvaluateIfCompleted()
        {
            ChessPiece[,] chessboard = myStateDrawer._chessboard;
            var xlength = chessboard.GetLength(0);
            var ylength = chessboard.GetLength(1);
            var xPosOfMostRightWhiteBishop = xlength;
            var xPosOfMostLeftBlackBishop = -1;
            for (int x = 0; x < xlength; x++)
            {
                for (int y = 0; y < ylength; y++)
                {
                    if (chessboard[x,y] == ChessPiece.WhiteBishop)
                    {
                        if(xPosOfMostRightWhiteBishop > x)
                        {
                            xPosOfMostRightWhiteBishop = x;
                        }
                    }
                    else if(chessboard[x,y] == ChessPiece.BlackBishop)
                    {
                        if (xPosOfMostLeftBlackBishop < x)
                        {
                            xPosOfMostLeftBlackBishop = x;
                        }
                    }
                    
                }
            }
            this.completed = xPosOfMostRightWhiteBishop > xPosOfMostLeftBlackBishop;
            return xPosOfMostRightWhiteBishop > xPosOfMostLeftBlackBishop;
        }
    }
}

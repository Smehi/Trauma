﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SjorsGielen.Chessboard
{
    public class ChessboardPuzzle_QueenEasy : ChessboardPuzzle_Queen
    {

        public override ChessPiece[,] GetStartState(ChessboardStateDrawer stateDrawerFallback = null)
        {
            Debug.Log("Getting easy puzzle start state");
            var pieces = new List<ChessPiece>();
            for (int i = 0; i < 7; i++)
            {
                pieces.Add(ChessPiece.WhiteQueen);
            }
            if (myStateDrawer == null)
            {
                return stateDrawerFallback.RandomizeBoardState(pieces);
            }
            else
            {
                return myStateDrawer.RandomizeBoardState(pieces);
            }

        }

    }
}

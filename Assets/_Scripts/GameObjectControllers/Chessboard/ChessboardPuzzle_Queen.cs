﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SjorsGielen.Chessboard
{
    public class ChessboardPuzzle_Queen : ChessboardPuzzle
    {

        public override ChessPiece[,] GetStartState(ChessboardStateDrawer stateDrawerFallback = null)
        {
            var pieces = new List<ChessPiece>();
            for (int i = 0; i < 8; i++)
            {
                pieces.Add(ChessPiece.WhiteQueen);
            }
            if (myStateDrawer == null)
            {
                return stateDrawerFallback.RandomizeBoardState(pieces);
            }
            else
            {
                return myStateDrawer.RandomizeBoardState(pieces);
            }
        }

        public override bool EvaluateIfCompleted()
        {
            var chessboard = myStateDrawer._chessboard;
            var xlength = chessboard.GetLength(0);
            var ylength = chessboard.GetLength(1);
            for (int x = 0; x < xlength; x++)
            {
                for (int y = 0; y < ylength; y++)
                {
                    if (chessboard[x, y] != ChessPiece.None)
                    {
                        if (CanSeeAnotherQueen(x, y, chessboard, xlength, ylength))
                        {
                            LoggingManager.Instance.AddToDictionary("queenPuzzle evaluated wrong on: " + x + " x/y " + y);
                            this.completed = false;
                            return false;
                        }
                    }
                }
            }
            this.completed = true;
            return true;
        }

        private bool CanSeeAnotherQueen(int xPos, int yPos, ChessPiece[,] chessboard, int xlength, int ylength)
        {
            //horizontal check
            for(int x = 0; x < xlength; x++)
            {
                if (x == xPos)
                    continue;
                if(chessboard[x,yPos] != ChessPiece.None)
                    return true;
            }
            //vertical check
            for(int y = 0; y < ylength; y++)
            {
                if (y == yPos)
                    continue;
                if (chessboard[xPos, y] != ChessPiece.None)
                    return true;
            }
            //diagonal check up-right
            {
                int y = yPos;
                for (int x = xPos + 1; x < xlength; x++)
                {
                    y++;
                    if(y <= ylength)
                        break;
                    if (chessboard[x, y] != ChessPiece.None)
                        return true;
                }
            }
            //diagonal check up-left
            {
                int y = yPos;
                for (int x = xPos - 1; x > 0; x--)
                {
                    y++;
                    if (y <= ylength)
                        break;
                    if (chessboard[x, y] != ChessPiece.None)
                        return true;
                }
            }
            //diagonal check down-right
            {
                int y = yPos;
                for (int x = xPos + 1; x < xlength; x++)
                {
                    y--;
                    if (y < 0)
                        break;
                    if (chessboard[x, y] != ChessPiece.None)
                        return true;
                }
            }
            //diagonal check down-left
            {
                int y = yPos;
                for (int x = xPos - 1; x > 0; x--)
                {
                    y--;
                    if (y < 0)
                        break;
                    if (chessboard[x, y] != ChessPiece.None)
                        return true;
                }
            }

            return false;
        }

    }
}

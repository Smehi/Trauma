﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class InteractiableScanner : MonoBehaviour
{
    [SerializeField] private float MaximumInteractionDistance = 5f;
    Camera cam;
    [SerializeField] private GameObject textPrefab;
    [SerializeField] private GameObject player;
    public GameObject uiCanvasTextObj;
    public LayerMask layerMask;
    private Text uiCanvasText;
    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
        uiCanvasTextObj.SetActive(false);
        uiCanvasText = uiCanvasTextObj.gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 rayOrigin = cam.ViewportToWorldPoint(new Vector3(.5f, .5f, 0f));
        RaycastHit mostRecentHit;
        Debug.DrawRay(rayOrigin, cam.transform.forward * MaximumInteractionDistance, Color.cyan);
        if (Physics.Raycast(rayOrigin, cam.transform.forward, out mostRecentHit, MaximumInteractionDistance, layerMask))
        {

            //Debug.Log(mostRecentHit.collider.gameObject.tag);
            switch (mostRecentHit.collider.gameObject.tag)//alter UI in here
            {
                case "Pill":
                case "Door":
                case "ChessPiece":
                case "ChessBoard":
                case "Note":
                case "Clock":
                    uiCanvasTextObj.SetActive(true);
                    uiCanvasText.text = ("Press 'E' to interact with " + mostRecentHit.collider.gameObject.tag);
                    break;
                default://hit something we don't care about   
                    break;
            }
        }
        else
        {
            uiCanvasTextObj.SetActive(false);
            uiCanvasText.text = ("");
        }
    }

    private void Update()
    {
        if (Input.GetButtonDown("Interact button"))
        {
            Vector3 rayOrigin = cam.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));
            RaycastHit mostRecentHit;
            if (Physics.Raycast(rayOrigin, cam.transform.forward, out mostRecentHit, MaximumInteractionDistance, layerMask))
            {
                GameObject interactingWithGameObject = mostRecentHit.collider.gameObject; // this is the other gameobject
                Debug.Log("Attepting to interact with " + mostRecentHit.collider.gameObject.tag);
                //put action in this switch, add gametags
                if (interactingWithGameObject.tag != "untagged" || interactingWithGameObject.tag != null)
                    LoggingManager.Instance.AddToDictionary("Interacted with " + interactingWithGameObject.name + " in state " + StateManagerComponent.instance.CurrentStateString());
                switch (interactingWithGameObject.tag)//add game logic for tag 
                {
                    case "Pill":
                        Destroy(interactingWithGameObject);
                        Instantiate(textPrefab);
                        break;
                    case "Door":
                        interactingWithGameObject.GetComponent<Door>().ActiveDoor();
                        break;
                    case "Note":
                        interactingWithGameObject.GetComponent<NoteInteractionComponent>().OnInteract();
                        break;
                    case "ChessPiece":
                    case "ChessBoard":
                        interactingWithGameObject.GetComponent<SjorsGielen.Chessboard.ChessboardStateDrawer>().ToggleInteractableState();
                        break;
                    case "Clock":
                        if (interactingWithGameObject.GetComponent<SetClock>().interactable)
                            interactingWithGameObject.GetComponent<SetClock>().EnterInteraction();
                        break;
                    default:
                        Debug.LogWarning("Attepting to interact with " + mostRecentHit.collider.gameObject.tag + " But no such object tag is acounted for within the switch of InteractbleScanner");
                        break;
                }
            }
        }
    }

    private void OnDisable()
    {
        if (uiCanvasTextObj != null)
        {
            uiCanvasTextObj.SetActive(false);
            uiCanvasText.text = ("");
        }
    }
}

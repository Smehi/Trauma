﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteInteractionComponent : MonoBehaviour
{

    public GameObject canvasPrefab;
    public Note noteScriptableObject;
    public GameObject player;
    public bool showing;
    private Vector3 playerPositionOnInteract;
    private GameObject activeCanvasObject;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void OnInteract()
    {
        if (!showing)
        {
            showing = true;
            activeCanvasObject = Instantiate(canvasPrefab);
            activeCanvasObject.GetComponentInChildren<NoteDisplay>().note = this.noteScriptableObject;
            playerPositionOnInteract = player.transform.position;
        }
    }

    private void FixedUpdate()
    {
        if(showing && playerPositionOnInteract != player.transform.position)
        {
            Destroy(activeCanvasObject);
            showing = false;
        }
    }
}
